module.exports = {
    root: true,
    extends: "eslint:recommended",
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        jquery: true
    },
    globals: {},
    rules: {
        indent: ["error", 4],
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "double"],
        semi: ["error", "always"],
        "func-style": "off",
        "global-require": "off",
        "no-bitwise": "off",
        "no-plusplus": "off",
        "no-unneeded-ternary": "off",
        "prefer-const": "off",
        "prefer-spread": "off",
        radix: ["error", "always"],
        strict: ["error", "global"],
        "eol-last": ["error", "always"]
    },
    settings: {
        "import/resolver": {
            "babel-module": {}
        }
    }
};
