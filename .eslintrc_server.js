module.exports = {
    root: true,
    extends: "eslint:recommended",
    env: {
        commonjs: true
    },
    globals: {
        dw: true,
        customer: true,
        session: true,
        request: true,
        response: true,
        empty: true,
        PIPELET_ERROR: true,
        PIPELET_NEXT: true
    },
    rules: {
        indent: ["error", 4],
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "double"],
        semi: ["error", "always"],
        "func-style": "off",
        "global-require": "off",
        "no-bitwise": "off",
        "no-plusplus": "off",
        "no-unneeded-ternary": "off",
        "prefer-const": "off",
        "prefer-spread": "off",
        radix: ["error", "always"],
        strict: ["error", "global"],
        "eol-last": ["error", "always"]
    }
};
